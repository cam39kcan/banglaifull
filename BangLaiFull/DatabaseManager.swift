//
//  DatabaseManager.swift
//  BangLaiFull
//
//  Created by Đỗ Hồng Quân on 10/13/18.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit
import FMDB

class DatabaseManager {
    static let shared = DatabaseManager()
    var path1: String?
    var path2: String?
    
    init() {
        path1 = copyDatabaseIfNeeded1()
        path2 = copyDatabaseIfNeeded2()
    }
    
    func copyDatabaseIfNeeded1() -> String? {
        let fileManager = FileManager.default
        let documentUrl = fileManager.urls(for: .documentDirectory, in: .allDomainsMask)
        guard documentUrl.count != 0 else {
            return nil
        }
        let finalDatabaseURL = documentUrl.first!.appendingPathComponent(AppConstants.databaseFile1)
        if !((try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
            print("Not exist!")
            let documentsURL = Bundle.main.resourceURL?.appendingPathComponent(AppConstants.databaseFile1)
            do {
                try fileManager.copyItem(atPath: (documentsURL?.path)!, toPath: finalDatabaseURL.path)
                return finalDatabaseURL.path
            } catch _ as NSError {
                print("Cant copy")
            }
        }
        else {
            print("Duong dan: \(finalDatabaseURL.path)")
            return finalDatabaseURL.path
        }
        return nil
    }
    
    func copyDatabaseIfNeeded2() -> String? {
        let fileManager = FileManager.default
        let documentUrl = fileManager.urls(for: .documentDirectory, in: .allDomainsMask)
        guard documentUrl.count != 0 else {
            return nil
        }
        let finalDatabaseURL = documentUrl.first!.appendingPathComponent(AppConstants.databaseFile2)
        if !((try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
            print("Not exist!")
            
            let documentsURL = Bundle.main.resourceURL?.appendingPathComponent(AppConstants.databaseFile2)
            do {
                try fileManager.copyItem(atPath: (documentsURL?.path)!, toPath: finalDatabaseURL.path)
                return finalDatabaseURL.path
            } catch _ as NSError {
                print("Cant copy")
            }
        }
        else {
            print("Duong dan: \(finalDatabaseURL.path)")
            return finalDatabaseURL.path
        }
        return nil
    }
    
    func getAllCauHoi() -> [CauHoi] {
        var listCauHoi = [CauHoi]()
        if let path = path2 {
            let CauHoiDb = FMDatabase(path: path)
            if CauHoiDb.open() {
                let query = "SELECT * FROM CAUHOI"
                do {
                    let result = try CauHoiDb.executeQuery(query, values: nil)
                    while result.next() {
                        let cauhoi = CauHoi()
                        cauhoi.id = Int(result.int(forColumn: "ID"))
                        cauhoi.cauHoi = result.string(forColumn: "CAUHOI")
                        cauhoi.anh = Int(result.int(forColumn: "ANH"))
                        cauhoi.A = result.string(forColumn: "A")
                        cauhoi.B = result.string(forColumn: "B")
                        cauhoi.C = result.string(forColumn: "C")
                        cauhoi.D = result.string(forColumn: "D")
                        cauhoi.dapAn = result.string(forColumn: "DAPAN")
                        cauhoi.loaiBang = Int(result.int(forColumn: "LOAIBANG"))
                        cauhoi.loaiBang2 = Int(result.int(forColumn: "LOAIBANG2"))
                        listCauHoi.append(cauhoi)
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
            }
        }
        return listCauHoi
    }
}
